package org.budo.warehouse.web.api;

import org.budo.warehouse.logic.api.DataMessage;

/**
 * @author lmw
 */
public interface DataMessageApi {
    void post(DataMessage dataMessage);
}