package org.budo.warehouse.logic.api;

/**
 * @author lmw
 */
public interface DataProducerLoader {
    void loadDataProducer();

    void restartThread();
}