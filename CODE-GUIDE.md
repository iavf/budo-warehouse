# 代码导读

## 程序启动时加载数据入口

```
// 根据数据库中的配置加载各个数据输入通道
org.budo.warehouse.logic.producer.DataProducerLoaderImpl.loadDataProducer()
```
```
// canal 读取 MySQL Binlog 的方式
org.budo.warehouse.logic.bean.LogicDynamicBeanProvider.canalDataProducer(DataNode)
```
```
// 收 Kafka/MQ 消息的方式
org.budo.warehouse.logic.bean.LogicDynamicBeanProvider.asyncDataProducer(DataNode, Pipeline)
```
```
// 标准格式 HTTP POST
org.budo.warehouse.web.impl.DataMessageApiImpl
```
```
// 适配格式 HTTP POST
org.budo.warehouse.web.controller.WebhookController
```


## 收到数据后进入分发
```
// 处理分发相关逻辑
org.budo.warehouse.logic.consumer.DispatcherDataConsumer.consume(DataMessage)
```
```
// 筛选单个通道想要的数据
org.budo.warehouse.logic.api.IEventFilterLogic.filter(Pipeline, DataMessage)
```

## 处理数据列变更（增加一个列或处理列的值）
```
// 将数据包装起来
org.budo.warehouse.logic.bean.LogicDynamicBeanProvider.mappingConsumerWrapper(DataConsumer)

org.budo.warehouse.logic.consumer.mapping.FieldMappingDataConsumerWrapper
```
```
// 经过包装后的取值逻辑
org.budo.warehouse.logic.consumer.FieldMappingDataEntry
```

## 以不同方式消费数据
```
// 消费接口
org.budo.warehouse.logic.api.DataConsumer.consume(DataMessage)
```
```
// 方式1 JDBC写库
org.budo.warehouse.logic.consumer.jdbc.JdbcDataConsumer

org.budo.warehouse.logic.consumer.jdbc.MysqlDataConsumer
```
```
// 方式2 发送Kafka或MQ消息
org.budo.warehouse.logic.consumer.async.AsyncDataConsumerWrapper

org.budo.warehouse.logic.bean.LogicDynamicBeanProvider.asyncConsumerWrapper(DataConsumer)

org.budo.warehouse.logic.bean.LogicDynamicBeanProvider.asyncDataConsumer(Pipeline)
```
```
// 方式3 将变更以邮件方式发送
org.budo.warehouse.logic.consumer.mail.MailDataConsumer
```
