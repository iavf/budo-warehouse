package org.budo.warehouse.service.api;

import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author lmw
 */
public interface JdbcExecuteService {
    Integer executeUpdate(Pipeline pipeline, String sql, Object[] parameters);
}