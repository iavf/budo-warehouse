package org.budo.warehouse.service.api;

import javax.sql.DataSource;

import org.budo.warehouse.service.entity.DataNode;

/**
 * @author lmw
 */
public interface ServiceDynamicBeanProvider {
    DataSource dataSource(DataNode dataNode);

    DataSource dataSource(DataNode dataNode, String schema);
}