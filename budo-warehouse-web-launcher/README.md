# budo-warehouse-web-launcher

## 开发环境启动

运行`budo-warehouse-web-launcher`中`org.budo.warehouse.web.launcher.Application`的`main`方法

## 配置文件

配置文件放在`C:\.budo.config\.config.cache\.budo-warehouse-web-launcher.properties`，C为当前磁盘，如果项目运行在D盘则为D

Linux环境则为 `\.budo.config\.config.cache\.budo-warehouse-web-launcher.properties`

内容如下

```
// 本地开发测试时存储配置信息的数据库
db.url=jdbc:mysql://127.0.0.1/budo_warehouse_100
db.username=root
db.password=
// 暂固定为N/A
dubbo.registry.address=N/A
```


## 生产环境构建

```
待补充
```

## 生产环境启动

```
待补充
```
