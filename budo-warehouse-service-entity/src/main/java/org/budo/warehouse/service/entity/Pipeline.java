package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 一个数据同步通道
 * 
 * @author limingwei
 */
@Getter
@Setter
@Accessors(chain = true)
@ToString
@Table(name = "t_pipeline")
public class Pipeline implements Serializable {
    private static final long serialVersionUID = -8308301956037787522L;

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String description;

    @Column(name = "event_filter")
    private String eventFilter;

    @Column(name = "source_datanode_id")
    private Integer sourceDataNodeId;

    @Column(name = "source_schema")
    private String sourceSchema;

    @Column(name = "source_table")
    private String sourceTable;

    @Column(name = "target_datanode_id")
    private Integer targetDataNodeId;

    @Column(name = "target_schema")
    private String targetSchema;

    @Column(name = "target_table")
    private String targetTable;

    /**
     * 是否已删除，未删除时为空
     */
    @Column(name = "deleted_at")
    private Timestamp deletedAt;
}